package ru.yandex;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import static org.junit.Assert.*;

public class EventsStatTest {
    private EventsStat eventsStat;

    @Before
    public void beforConstruction() {
        eventsStat = new EventStatImpl();
        eventsStat.recordEvent(System.currentTimeMillis() / 1000); // Add one event before passing the tests
    }

    @After
    public void afterTest() {
        ((EventStatImpl) eventsStat).shutdownService();
    }

    @Test
    public void recordOneEvent() {
        assertTrue(eventsStat.recordEvent(System.currentTimeMillis() / 1000));
        assertEquals(2, eventsStat.getCountEventsMinute());
        assertEquals(2, eventsStat.getCountEventsHour());
        assertEquals(2, eventsStat.getCountEventsDay());
    }

    @Test
    public void recordNegativeEvent() {
        assertFalse(eventsStat.recordEvent(-1000));
        assertEquals(1, eventsStat.getCountEventsMinute());
        assertEquals(1, eventsStat.getCountEventsHour());
        assertEquals(1, eventsStat.getCountEventsDay());
    }

    @Test
    public void recordEventFromPast() {
        assertFalse(eventsStat.recordEvent(System.currentTimeMillis() / 1000 - TimeInterval.DAY_SECONDS.getSecondsValue() * 2));
        assertEquals(1, eventsStat.getCountEventsMinute());
        assertEquals(1, eventsStat.getCountEventsHour());
        assertEquals(1, eventsStat.getCountEventsDay());
    }

    @Test
    public void recordEventFromFuture() {
        assertFalse(eventsStat.recordEvent(System.currentTimeMillis() / 1000 + 1000));
        assertEquals(1, eventsStat.getCountEventsMinute());
        assertEquals(1, eventsStat.getCountEventsHour());
        assertEquals(1, eventsStat.getCountEventsDay());
    }

    @Test
    public void recordTenThousandEvent() {
        for (int i = 0; i < 10000; i++) {
            eventsStat.recordEvent(System.currentTimeMillis() / 1000);
        }

        assertEquals(10001, eventsStat.getCountEventsMinute());
        assertEquals(10001, eventsStat.getCountEventsHour());
        assertEquals(10001, eventsStat.getCountEventsDay());
    }

    @Test
    public void recordTenThousandOldEvent() {
        for (int i = 0; i < 10000; i++) {
            eventsStat.recordEvent(System.currentTimeMillis() / 1000 - TimeInterval.DAY_SECONDS.getSecondsValue() * 2);
        }

        assertEquals(1, eventsStat.getCountEventsMinute());
        assertEquals(1, eventsStat.getCountEventsHour());
        assertEquals(1, eventsStat.getCountEventsDay());
    }

    /**
     * Test with а delay to check the function getCountEventsInLastMinute
     */
    @Test
    public void recordOneEventWithDelay() {
        assertEquals(1, eventsStat.getCountEventsMinute());

        try {
            Thread.sleep(65000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals(0, eventsStat.getCountEventsMinute());
    }

    @Test
    public void recordOneEventAndTestCleanService(){
        assertTrue(eventsStat.recordEvent(System.currentTimeMillis() / 1000 - TimeInterval.DAY_SECONDS.getSecondsValue()+5));
        assertEquals(2, eventsStat.getCountEventsDay());

        try {
            Thread.sleep(65000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals(1, eventsStat.getCountEventsDay());
    }

    /**
     * Test with multi-threaded query
     */
    @Test
    public void recordMultipleTaskEvent() {
        AtomicLong timeNow = new AtomicLong(System.currentTimeMillis() / 1000);

        ExecutorService service = Executors.newCachedThreadPool();

        for (int i = 0; i < 10000; i++) {
            service.submit(() -> {
                eventsStat.recordEvent(timeNow.get());
            });
        }

        try {
            service.shutdown();
            service.awaitTermination(10L, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            service.shutdownNow();
        }

        assertEquals(10001, eventsStat.getCountEventsMinute());
        assertEquals(10001, eventsStat.getCountEventsHour());
        assertEquals(10001, eventsStat.getCountEventsDay());
    }
}
