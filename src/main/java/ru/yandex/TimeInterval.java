package ru.yandex;

/**
 * Time that can be used for build interval and etc.
 */
public enum TimeInterval {
    MINUTE_SECONDS(60),
    HOUR_SECONDS(3_600),
    DAY_SECONDS(86_400);

    private int seconds;

    TimeInterval(int seconds) {
        this.seconds = seconds;
    }

    public int getSecondsValue() {
        return seconds;
    }
}
