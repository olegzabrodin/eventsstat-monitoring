package ru.yandex;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;


/**
 * Class event monitoring and statistic
 * Events stored in ConcurrentHashMap<K, V>:
 * K - key is events time in seconds.
 * V - value is a number of events for the same time.
 * <p>
 * Event statistics are provided for the last day (24 hours).
 * Events that occurred more than a day ago are deleted by the old data cleaning service.
 */
public class EventStatImpl implements EventsStat {
    /**
     * Events are stored in ConcurrentHashMap
     */
    private final Map<Long, Long> eventsMap;

    /**
     * Service for clean old data in eventsMap
     */
    private final ScheduledExecutorService cleanService;

    EventStatImpl() {

        this.eventsMap = new ConcurrentHashMap<Long, Long>();
        this.cleanService = Executors.newSingleThreadScheduledExecutor();

        // Every 60 seconds we run the data cleaning task
        cleanService.schedule(new Runnable() {
            public void run() {
                if (eventsMap.isEmpty()) return; // if eventsMap clear, we don't need to clean data

                // Calculate the time that is not involved in the statistics
                AtomicLong oldestEventTime = new AtomicLong(getCurrentTimeInSecond() - TimeInterval.DAY_SECONDS.getSecondsValue());
                // Convert hash into stream, filter and delete items
                eventsMap.keySet()
                        .stream()
                        .filter(time -> time < oldestEventTime.get())
                        .forEach(eventsMap::remove);
            }
        }, 60, TimeUnit.SECONDS);
    }

    public boolean recordEvent(long eventTime) {
        AtomicLong beginTime = new AtomicLong(getCurrentTimeInSecond() - TimeInterval.DAY_SECONDS.getSecondsValue());

        if (eventTime >= beginTime.get() && eventTime <= getCurrentTimeInSecond()) {
            eventsMap.compute(eventTime, (time, count) -> count == null ? 1L : count + 1);
            return true;
        }
        return false;
    }

    public long getCountEventsMinute() {
        return getEventsByInterval(TimeInterval.MINUTE_SECONDS.getSecondsValue());
    }

    public long getCountEventsHour() {
        return getEventsByInterval(TimeInterval.HOUR_SECONDS.getSecondsValue());
    }

    public long getCountEventsDay() {
        return getEventsByInterval(TimeInterval.DAY_SECONDS.getSecondsValue());
    }

    /**
     * Base function to return the number
     * of events in a given interval (seconds)
     *
     * @param interval positive value in seconds
     * @return number of events
     */
    private long getEventsByInterval(long interval) {
        if (interval <= 0) return 0;

        AtomicLong timeNow = new AtomicLong(getCurrentTimeInSecond());
        AtomicLong beginTime = new AtomicLong(timeNow.get() - interval);

        AtomicLong count = new AtomicLong(0);
        for (Long time : eventsMap.keySet()) {
            if (time >= beginTime.get() && time <= timeNow.get()) {
                long l = eventsMap.get(time);
                count.addAndGet(l);
            }
        }
        return count.get();
    }

    /**
     * @return current time in seconds
     */
    private Long getCurrentTimeInSecond() {
        return System.currentTimeMillis() / 1000;
    }

    /**
     * Shutdown the service who clean old data
     */
    void shutdownService() {
        try {
            cleanService.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cleanService.shutdownNow();
        }
    }
}
