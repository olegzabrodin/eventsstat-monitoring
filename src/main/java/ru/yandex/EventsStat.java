package ru.yandex;

public interface EventsStat {

    /**
     * Record an event
     *
     * @param eventTime event time in seconds.
     */
    boolean recordEvent(long eventTime);

    /**
     * @return number of events in the last minute
     */
    long getCountEventsMinute();

    /**
     * @return number of events in the last hour
     */
    long getCountEventsHour();

    /**
     * @return number of events in the last day
     */
    long getCountEventsDay();
}